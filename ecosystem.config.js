module.exports = {
	apps:[{
		name: 'vz5ghirouterwatchdog',
		script: 'index.js',
		args: '',
		autorestart: true,
		max_memory_restart: '200M',
		env: {
		},
		watch: true,
	}]
};
