import asyncio
import configparser
import os
import time
import urllib.request

import kasa 

testHosts = []
sleepIntv = {}
tgtPlugs  = {}
pcCount   = 0
tLastPc   = -1

async def powercycleoneplug(plg, intv):
	await plg.update()
	await plg.turn_off()
	await asyncio.sleep(intv)
	await plg.turn_on()

def powercycle(intv):
	fn = 'powercycle'

	print(fn, intv)
	
	for tgtPlg in tgtPlugs:
		try:
			print(fn, "cycling ", tgtPlg, ":", tgtPlugs[tgtPlg], " for ", intv, "s")
			plg = kasa.SmartPlug(tgtPlugs[tgtPlg])
			if plg == None:
				print(fn, tgtPlg, ":", tgtPlugs[tgtPlg], " not found")
				return
			asyncio.run(powercycleoneplug(plg, intv))
		except Exception:
			print(fn, Exception)

	# sleep a lot longer after every 3rd hit to limit power cycles
	si = sleepIntv['post']
	if pcCount>0 and pcCount%3 == 0:
		si = si*6
	print(fn, 'post power cycle sleep for ', si, 's')
	time.sleep(si)

def isConnected(h, https=True):
	fn = 'isConnected'

	rtn = True
	sc  = -1
	try:
		upfx = 'https://'
		if https == False:
			upfx = 'http://'
		x=urllib.request.urlopen(url=upfx+h, timeout=8)
		sc = x.getcode()
		# if sc < 200 or sc > 299:
		# 	rtn = False
	except Exception:
		print(fn, Exception)
		rtn = False
	
	print(fn, 'h=', h, 'sc=', sc, 'rtn=', rtn, 'pcCount=', pcCount)
	return rtn

def main():
	cfg = configparser.ConfigParser()
	cfl = 'cfg.ini'
	if ('CFG' in os.environ):
		cfl = os.environ['CFG']
	cfg.read(cfl)

	testHosts = cfg['TESTHOSTS']['v'].split(',')
	localNet  = cfg['LOCALNET']['v'].split(',')
	print('testHosts', testHosts, 'localNet', localNet)

	sleepIntv['poll'] = int(cfg['SLEEP']['poll'])
	sleepIntv['flip'] = int(cfg['SLEEP']['flip'])
	sleepIntv['post'] = int(cfg['SLEEP']['post'])
	print('sleepIntv', sleepIntv)

	tgtplgs = cfg['PLUGS']['v'].split(',')
	allplgs = asyncio.run(kasa.Discover.discover())
	for ip in allplgs:
		alias = allplgs[ip].alias
		if alias in tgtplgs:
			tgtPlugs[alias] = ip
	print('tgtPlugs', tgtPlugs)

	hi = 0
	while True:
		hi = (hi+1)%len(testHosts)
		h  = testHosts[hi]
		c  = isConnected(h)
		
		if (c == False):
			# check to make sure that the local network is up
			localNetUp = True
			for ln in localNet:
				localNetUp = localNetUp and isConnected(ln, https=False)
			
			if (localNetUp == True):
				print('local network is up - powercycling')
				powercycle(sleepIntv['flip'])
				global pcCount
				pcCount = pcCount + 1
			else:
				print('local network is down - ignoring')

		time.sleep(sleepIntv['poll'])

main()
