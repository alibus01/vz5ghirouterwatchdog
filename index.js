const axios = require('axios');
const http  = require('http');
const plyrt = require('playwright');
const cfg   = require(process.env.CFGJSON||'./config.json');

// setup defaults
if (!cfg.testhosts) {cfg.testhosts = ["www.bing.com", "www.google.com"];}
cfg.testhostIdx = 0;
cfg.routerhost = cfg.routerhost||'192.168.0.1';
cfg.timeoutsInSec = cfg.timeoutsInSec||{connect:2, heartbeat:36, waitAfterOp:24, waitAfterRouterConnFail:90};

const status = {lastFailed:null, failCount:0, passCount:0, routerConnFail:false, status:null, logExcerpt:''};
const setSts = (o) => {
	for (let k in o) {
		status[k] = o[k];
	}
}

console.log({cfg});

const initConn = async () => {
	const fn = `initConn`;

	try {
		const br = await plyrt.firefox.launch();
		const pg = await br.newPage();

		await pg.goto(`http://${cfg.routerhost}/udm/login.html`);
		await pg.waitForLoadState('load');

		await pg.locator('input[type="password"]').fill(cfg.routerpass);
		await pg.click('button:has-text("Sign in")');
		await pg.waitForLoadState('load');

		await pg.waitForNavigation(/*{ url: 'http://192.168.0.1/udm/user/index.html#home/home.html' }*/);
		await pg.locator('#id_menu_network').click();
		await pg.locator('#id_menu_lte').click();
		// await pg.screenshot({path:'./a0.png'});

		status.routerConnFail = false;

		return {br, pg};
	} catch (e) {
		status.routerConnFail = true;
		console.error({fn, message:`0: unexpected error ${e}`});
		throw (e);
	} finally {
		console.log({fn});
	}
}

const k = {Connect:'Connect', Disconnect:'Disconnect'};

const kwOpposite = (kw) => {
	const fn = 'kwOpposite';
	switch(kw) {
	case k.Connect:
		return k.Disconnect;
	case k.Disconnect:
		return k.Connect;
	default:
		throw new Error(`${fn} message:'unexpected kw:${kw}'`);
	}
}

const kwTimeout = (kw) => {
	const fn = 'kwTimeout';
	let v = 36;
	switch(kw) {
	case k.Connect:
		v = cfg.timeoutsInSec.waitAfterConnect||cfg.timeoutsInSec.waitAfterOp;
		break;
	case k.Disconnect:
		v = cfg.timeoutsInSec.waitAfterDisconnect||cfg.timeoutsInSec.waitAfterOp;
		break;
	default:
		throw new Error(`${fn} message:'unexpected kw:${kw}'`);
	}
	return v*1000;
}

const delay = async (dly) => {
	return new Promise((v) => {
		setTimeout(() => {
			return v();
		}, dly);
	})
}

const perfOp = async (kw) => {
	const fn = 'perfOp';

	let o = {br:null, pg:null};
	try {
		if (!k[kw]) {throw(new Error(`unexpected kw:${kw}`));}

		console.log({fn, kw, message:'started'});
		o = await initConn();		

		// await o.pg.screenshot({path:`./a1-${kw}-0.png`});
		await o.pg.click(`button:has-text("${kw}")`, {timeout:4000});
		// await o.pg.screenshot({path:`./a1-${kw}-1.png`});
		
		await o.pg.locator(`#field_lte_net_status:has-text(${kwOpposite(kw)})`, {timeout:72000});
		await delay(kwTimeout(kw));
		// await o.pg.screenshot({path:`./a1-${kw}-2.png`});

		status.routerConnFail = false;
		console.log({fn, kw, message:'completed', waitInSec:kwTimeout(kw)});
	} catch (e) {
		status.routerConnFail = true;
		console.error({fn, kw, message:`unexpected error ${e}`});
	} finally {
		if (o.br) {await o.br.close();}
	}
};

const main = async () => {
	const fn = 'main';

	let didFail = false;
	try {
		cfg.testhostIdx = (++cfg.testhostIdx%cfg.testhosts.length);

		const opts = {url:`https://${cfg.testhosts[cfg.testhostIdx]}/`, method:'HEAD', timeout:cfg.timeoutsInSec.connect*1000};
		const req = await axios(opts);
		setSts({passCount:++status.passCount, status:'UP'});
		console.log({h:cfg.testhosts[cfg.testhostIdx], sc:req.status});		
	} catch(e) {
		didFail = true;
		setSts({failCount:++status.failCount, lastFailed:(new Date()), status:'DOWN'});
		console.error({fn, e:e.message, host:cfg.testhosts[cfg.testhostIdx], message:`connection failed, resetting`});
		try {
			// console.log({fn, message:`remediation disabled`});
			await perfOp(k.Disconnect);
			await perfOp(k.Connect);
		} catch (e) {
			console.error({fn, e, message:`reset failed`});
		}
	} finally {
		const wait = (status.routerConnFail?cfg.timeoutsInSec.waitAfterRouterConnFail:didFail?cfg.timeoutsInSec.connect:cfg.timeoutsInSec.heartbeat)*1000;
		console.log({fn, message:'starting next cycle', wait});
		status.routerConnFail = false;
		didFail = false;
		setTimeout(main, wait);
	}
};

main()
.catch((e) => {
	console.error({message:`final error ${e}`});
});

// create a cheap and quick service to easily get status
if (cfg.statusPort > 0) {
	const svr = http.createServer();

	svr.on('request', (q,r) => {
		r.writeHead(200, {'content-type':'application/json'});
		r.end(JSON.stringify(status));
	});

	svr.listen(cfg.statusPort);
}
