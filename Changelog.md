# Changelog

## 0.2.2:
* Change script names in `package.json` so that `pm2` operations start with that as a prefix.

## 0.2.1
* Print out the hostname that failed the last check.
* Comment out a remaining Playwright `screenshot` call (TODO: hide these calls behind a debug flag.)
* Add scripts in `package.json` to show logs and to stop/restart `pm2`.

## 0.2.0
* Add separate waits for `connect` and `disconnect` with fallback.
* Replace `currently` in web service output with `status`.

## 0.1.0
* Update `playwright`.
* Add a simple http service to enable status reporting - define `statusPort` in the config to enable it.

## 0.0.4
* Fix Changelog title formatting.
* After failure remediation attempt, try connectivity faster than the wait specified by `timeoutsInSec.heartbeat`.

## 0.0.3
* Moved timeouts in separate config.
* Better logging.

## 0.0.2
* Changed the code quite a bit to add delays and to re-establish the connection for the disconnect and connect cycles respectively to improve reliability of operation.

## 0.0.1
* Initial version.
