# Verizon 5G Home Internet Watchdog
This utility interacts with the Verizon ASK-NQ1338 (white cube) router to reset cellular functionality when it silently drops internet functionality. 

## Running
* Ensure that you have the latest NodeJS LTS (https://nodejs.org)
* Clone the source in a directory on the machine on which you intend to run. It would be best to do this on a machine that is on throughout the day
* Copy `config.json.tmpl` to `config.json` and edit its settings to match your environment. Important settings include the `routerhost` (typically 192.168.0.1) and `routerpass` (typically printed on the router)
* Run `npm install`
* Run `npm run start`

## Notes
* I've noticed that the `playwright` library tends to leak memory over long periods, which is why I now recommend using `cron` to periodically restart the process as `pm2 restart 0`.
* I've started working on a Python 3 script called `powercycleondisconnect.py` in this repo that attempts to power cycle the router instead by using the TP-Link Kasa library. This is experimental and will only work if you have a configured Kasa plug into which you have plugged in the Verizon router to support power cycling; I've found that this sometimes brings the connection back faster that trying to cycle through disconnect and connect. It is still a work in progress.
